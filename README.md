STEP 1:

run webserver_ssl_keygen and certbot to generate ssl key

STEP 2: 

Once key is generated, comment out webserver_ssl_keygen (and certbot, optional) so that webserver only runs

STEP 3:

to regen key (after 60 days), make sure certbot is uncommented and dependent on webserver and run both




run docker compose:
`sudo docker-compose up -d`

docker compse logs
`sudo docker-compose logs`

docker compose certbot
`sudo docker-compose up -d certbot`

docker compose webserver
`sudo docker-compose up -d webserver`

docker look in webserver container
`docker-compose exec webserver ls -la /etc/letsencrypt/live`

just start webserver component:
`docker-compose up -d --force-recreate --no-deps webserver`

get new certbot credentials
`docker-compose up --force-recreate --no-deps certbot`

generate ssl key for each domain :
`sudo openssl dhparam -out /home/ben/nginxdockerssl/dhparam/dhparam-2048.pem 2048`


## RUNNING DOCKER COMPOSE (keygen and ssl files)
* Use the ‘-f’ flag for custom file names (not docker-compose.yml): `docker-compose -f docker-compose.test.yml up`

