// https://benflatau.com/joeflatau/20s/scan0051.jpg
function createPhoto(element){
    const mainContent = document.getElementById('main-content');
   
    const photoContainer = document.createElement("div");
    photoContainer.className = "photo-container";

    const joePhoto = document.createElement("img");
    joePhoto.className = "joe-photo";
    joePhoto.src = basePhotoURL + element.fileName;
    joePhoto.alt = element.descriptionText;

    const joePhotoOverlay = document.createElement("div");
    joePhotoOverlay.className = 'photo-overlay';

    const joePhotoOverlayText = document.createElement("div");
    joePhotoOverlayText.className = 'photo-overlay-text';
    // joePhotoOverlayText.innerText = photo.descriptionText;
    joePhotoOverlayText.innerText = element.fileName;

    joePhotoOverlay.appendChild(joePhotoOverlayText);
    photoContainer.appendChild(joePhoto);
    photoContainer.appendChild(joePhotoOverlay);
    mainContent.appendChild(photoContainer);
}

function generateStartingPhotos(){
    const startingPhotos = photoArray.slice(0, photoPosition);

    startingPhotos.forEach((photo)=> {
        createPhoto(photo);
    });
}

function startScrollListener(){
    
    document.addEventListener("scroll", function (event) {
        if(window.history.state === 'Photos'){
            // console.log(window.history.state, 'thi sis scroll state')
            generateMorePhotos(); 
        }
        // else{console.log('noooo')}
        
    }, false);
    //BENDO: remove listner on other states...

    
    // window.addEventListener('popstate', (event) => {
    //     console.log('pop stated!');
    //     document.removeEventListener("scroll", function (event) {
    //         console.log('listener removed!');
    //         generateMorePhotos(); 
    //     }, false);
    // });
}




function generateMorePhotos() {
    var lastDiv = document.querySelector("#main-content > div:last-child");
    var lastDivOffset = lastDiv.offsetTop + lastDiv.clientHeight;
    var pageOffset = window.pageYOffset + window.innerHeight;

    if (pageOffset > lastDivOffset - 10 && photoArray.length >= photoPosition + 3) {

        for (i = photoPosition ; (i < photoPosition + 3); i++) {
            createPhoto(photoArray[i]);   
        };
        generateMorePhotos();
        photoPosition += 3;
    }

    if (photoArray.length < photoPosition + 3 && photoArray.length !== photoPosition) {
        if(photoArray[photoPosition]){
            createPhoto(photoArray[photoPosition]);
            photoPosition += 1; 
        }    
    }

    if (photoArray.length === photoPosition) {
        return 
    }

};

let photoPosition = 6;

const basePhotoURL = 'https://joeflatau.com/images';

const photoArray = [

    ////BABY PHOTOS////
    {
    fileName: '/Baby/baby1.jpg',
    descriptionText: 'testing123' 
    },
    {
    fileName: '/Baby/baby2.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby3.jpg',
    descriptionText: 'testing123' 
    },
    {
    fileName: '/Baby/baby4.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby5.jpg',
    descriptionText: 'testing123' 
    },
    {
    fileName: '/Baby/baby6.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby7.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby8.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby9.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby10.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby11.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby12.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby13.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby14.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby15.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/Baby/baby16.jpg',
    descriptionText: 'testingabc' 
    },

    ////SCHOOL PHOTOS////

    {
    fileName: '/School/school1.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school2.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school3.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school4.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school5.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school6.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school7.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school8.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school9.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school10.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school11.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school12.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school13.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school14.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school15.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school16.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school17.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school18.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school19.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school20.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school21.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school22.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school23.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school24.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school25.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school26.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school27.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school28.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school29.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school30.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school31.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/School/school32.jpg',
    descriptionText: 'testingabc' 
    },

    ////20s PHOTOS////

    {
    fileName: '/20s/20s1.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/20s/20s2.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/20s/20s3.jpg',
    descriptionText: 'testingabc' 
    },

    ////30s PHOTOS////

    {
    fileName: '/30s/30s1.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s2.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s3.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s4.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s5.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s6.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s7.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s8.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s9.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s10.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s11.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s12.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s13.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s14.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s15.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s16.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s17.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s18.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s19.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s20.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s21.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s22.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s23.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s24.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s25.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s26.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s27.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s28.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s29.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s30.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s31.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s32.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s33.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s34.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s35.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s36.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s37.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/30s/30s38.jpg',
    descriptionText: 'testingabc' 
    },

    ////40s PHOTOS////
    {
    fileName: '/40s/40s1.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s2.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s3.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s4.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s5.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s6.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s7.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s8.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s9.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s10.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s11.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s12.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s13.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s14.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s15.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s16.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s17.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s18.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s19.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s20.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s21.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s22.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s23.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s24.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s25.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s26.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s27.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s28.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s29.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s30.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s31.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s32.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s33.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s34.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s35.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s36.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s37.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s38.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s39.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s40.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s41.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s42.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s43.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s44.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s45.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s46.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s47.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s48.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s49.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s50.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s51.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s52.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s53.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s54.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s55.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s56.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s57.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s58.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s59.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s60.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s61.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s62.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s63.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s64.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s65.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s66.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s67.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s68.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s69.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s70.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s71.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s72.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s73.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/40s/40s74.jpg',
    descriptionText: 'testingabc' 
    },
    

    ///50s PHOTOS///


    {
    fileName: '/50s/50s1.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s2.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s3.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s4.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s5.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s6.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s7.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s8.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s9.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s10.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s11.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s12.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s13.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s14.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s15.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s16.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s17.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s18.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s19.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s20.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s21.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s22.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s23.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s24.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s25.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s26.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s27.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s28.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s29.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s30.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s31.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s32.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s33.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s34.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s35.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s36.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s37.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s38.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s39.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s40.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s41.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s42.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s43.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s44.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s45.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s46.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s47.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s48.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s49.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s50.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s51.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s52.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s53.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s54.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s55.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s56.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s57.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s58.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s59.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s60.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s61.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s62.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s63.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s64.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s65.jpg',
    descriptionText: 'testingabc'
    },
    {
    fileName: '/50s/50s66.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s67.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s68.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s69.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s70.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s71.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s72.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s73.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s74.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s75.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s76.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s77.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s78.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s79.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s80.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s81.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s82.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s83.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s84.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s85.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s86.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s87.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s88.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s89.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s90.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s91.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s92.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s93.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s94.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s95.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s96.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s97.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s98.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s99.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s100.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s101.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s102.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s103.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s104.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s105.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s106.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s107.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s108.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s109.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s110.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s111.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s112.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s113.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s114.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s115.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s116.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s117.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s118.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s119.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s120.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s121.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s122.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s123.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s124.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s125.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s126.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s127.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s128.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s129.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s130.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s131.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s132.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s133.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s134.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s135.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s136.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s137.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s138.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s139.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s140.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s141.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s142.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s143.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s144.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s145.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s146.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s147.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s148.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s149.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s150.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s151.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s152.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s153.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s154.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s155.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s156.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s157.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s158.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s159.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s160.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s161.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s162.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s163.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s164.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s165.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s166.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s167.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s168.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s169.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s170.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s171.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s172.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s173.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s174.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s175.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s176.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s177.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s178.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s179.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s180.jpg',
    descriptionText: 'testingabc' 
    },
    {
    fileName: '/50s/50s181.jpg',
    descriptionText: 'testingabc' 
    },
]



