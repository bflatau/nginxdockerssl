//set initial history state...
// const state = 'Home';
// const title = 'ben-test-title';
// const url = '';
// history.pushState(state, title, url);


//create a list of views (the text has to match what's in the html!)
var viewList = [
    { viewName: 'Home', fileName: '../html/home.html', function: ()=> console.log('hi there')},
    { viewName: 'About Joe', fileName: '../html/about.html', function: ()=> console.log('hi there')},
    { viewName: 'Stories', fileName: '../html/stories.html', function: ()=> generateStories()},
    { viewName: 'Photos', fileName: '../html/photos.html', function: ()=> {generateStartingPhotos(), startScrollListener()} },
    { viewName: 'Videos', fileName: '../html/videos.html', function: ()=> console.log('hi there') },
    { viewName: 'Information', fileName: '../html/info.html', function: ()=> console.log('hi there') },
];


//probably better way to do this....
var loadHomePage = function(){
    {var xhr = new XMLHttpRequest();
    xhr.open('GET', '../html/home.html', true);
    xhr.onreadystatechange = function () {
        if (this.readyState !== 4) return;
        if (this.status !== 200) return; // or whatever error handling you want
        console.log(this.status);
        history.pushState('Home', '', '');
        document.getElementById('main-content').innerHTML = this.responseText;
    };
    xhr.send();}
}


//on click, cycle through 
var handleNavClick = function(element) {

    viewList.forEach(function(item) {
        if(item.viewName === element.target.innerText){
            // document.getElementById('main-content').src = item.fileName;
            var xhr = new XMLHttpRequest();
            xhr.open('GET', item.fileName, true);
            xhr.onreadystatechange = function () {
                if (this.readyState !== 4) return;
                if (this.status !== 200) return; // or whatever error handling you want
                console.log(this.status);
                document.getElementById('main-content').innerHTML = this.responseText;
                history.replaceState(item.viewName, '', '');
                item.function();
            };
            xhr.send();
            
        }
      });
    //BENDO: REMOVE AFTER TESTING
    console.log(element.target.innerText);
    console.log(window.history.state, 'this is nav state');
    
    //Add button press to history object...
    // const state = element.target.innerText;
    // const title = 'ben-test-title';
    // const url = '';
    // history.pushState(state, title, url)
};


//grab all the nav bar items and assign them a variable (this is an array)
var navItems = document.getElementsByClassName("nav-item");

//loop through the nav bar items and add an event listener to run a function on click
//this needs to be after the handleNavClick function (otherwise JS can't find it :( )
for (var i = 0; i < navItems.length; i++) {
    navItems[i].addEventListener('click', handleNavClick, false);
}



