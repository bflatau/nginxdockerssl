const storyData = [

    {
        text: `The first and only time I met Joe Flatau was at a seemingly impromptu crab feast in the neighborhood. These are apparently hosted quite regularly and Joe is an obvious life of the party. I witnessed this first hand, and this is a recurrent theme in reading the many stories below. As a college friend of Ben’s, I had been graciously invited by the family to join them for a weekend including the great night on the beach. Joe generously welcomed my fiancée and I to make ourselves at home immediately. He lent us their bikes, golf cart rides etc. to create a phenomenal time. The weekend was short-lived but the memory of Joe’s hospitality and ability to bring a smile to the entire neighborhood will always be remembered.`,
        author: 'Blake Watterworth'
    },

    {
        text: `Gail is not exaggerating about the very special crab feast Joe and Joyce provided the night before Josie and Ben's wedding (see below). It was wonderful, but we felt so guilty seeing them slaving away in the kitchen for most of the night! We were left thinking how great it would be to visit them in Maryland the next time we headed east to visit our son's family in Philadelphia. Obviously, we were deeply saddened by the grim news of Joe's passing and traveled to Maryland with heavy hearts. Our flight got in late in the evening and we were hungry, so I asked the receptionist for restaurant recommendations. Cracker Barrel, Denny's, Burger King...
        Was there something more local? And, to this she recommended some lounge and restaurant, and Andy and I knew this sounded pretty down-home, so we went for it. We walked into a very crowded bar and were told that they were about to close the restaurant. I think we must have looked pretty sad, because they let us in. After we ordered, we texted Josie that we had landed and where we were eating. She responded, "THAT'S WHERE JOE GOT THE CRAB CAKES! G&M Lounge and Restaurant.
        How did we (1) decide to stay in Linthicum Heights that night (2) ask just the right person for a restaurant recommendation (3) get seated despite the late hour (4) Order crab cakes, of course.
        Thanks, Joe!`,
        author: 'Teresa & Andy Nevitt'
    },

    {
        text: `On June 19, 2015 the night before Ben and Josie Flatau got married, Josie’s family had a party for them. This was at Josie’s family’s home in California. Joe had flown in a mega amount of containers of crab cake mixture to their home, enough for hundreds! Joyce and Joe were in the Nevitt’s kitchen making these crab cakes. Joyce was forming the cake and putting it on the cookie sheet and Joe was standing at the oven cooking them. Tray after tray were made. They did this for at least an hour and a half. After I ate one of these amazing crab cakes, I was walking around and realized that everyone was talking about the crab cakes! Kind of craving another, I thought (being the helpful person I am) that I should go inside and see if I can help them. Upon entering the kitchen I saw Joe slaving over the oven and Joyce still making the cakes.
        I said, “Joe did you know that everyone is talking about your crab cakes?”
        He said, “Really?”
        I said, “You have no idea. I was walking around and all that I hear is,” “ Did you have a crab cake? The crab cakes are amazing! Where are the crab cakes? Are they bringing more out? Where can I get one?”
        Joe is laughing at me telling him all of the comments. So, now the party for Ben and Josie has become a crab cake festival!
        Well, I generously offered to carry out trays of new ones and bring back the empty trays. Of course I did this to be helpful with no thoughts of getting another myself! Hmm… maybe.
        Joe wanted to share something he thought everyone would enjoy and they certainly did! The guests were still talking about it the next day! Dr. Nevitt even mentioned it in his speech to the bride and groom. You can hear him in the video on this site.
        This was just one memory I have of Joe. I met him around 1980. He was my brother in law for a couple of years, but he always was a friend. I have years of memories with Joe and Joyce. The thing I will always remember is his smile, his laugh, his dimples, and his kindness to me and my two children who love him dearly.
        Crab cakes are one of my favorite foods. I will never have one again and not think of Joe. Thanks Joe. You’re the best! Tracy, Brian and I will miss you! Love, Gail-ie OX`,
        author: 'Gail Wolf'
    },

    {
        text: `The last time I interacted with Joe was just two weeks before his passing. He had generously offered to pick up horse droppings on the beach for me! One of my horses had made a deposit that the rider missed and didn't pick up (as we usually do). Joe called me, but we were out of town at the time. Joe offered to go scoop up the offending offering and I told him that I owed him a batch of cookies (the same 'payment' he recieved after my horses put countless divets in his beautiful lawn when they got out shortly after we moved into the neighborhood). It took a few weeks, but I finally called and told him and Joyce that a fresh batch of cookies had just come out of the oven. Joyce invited me to join her on a walk with Ali at the same time, so I stopped by with the plate of cookies, which Joe threatened to gobble up with Patric while we were on the dog walk! Joyce and I had a great walk while Joe and Patrick stayed at home. I'm not sure if Joyce ever did get any of the cookies, but just hearing Joe's jovial voice up on the deck, joking about cookies while also sharing the cookes, while we were heading out with Ali is a memory that emboidies my memory of Joe. Joking, inclusive, caring. This was one of the good guys!`,
        author: 'Bea Wright'
    },

    {
        text: `One of my most recent memories of Joe was the neighborhood annual picnic. Joe suggested a theme, and, as always, he had his pulse on the heartbeat of the neighborhood. The neighborhood agreed that it was one of the most wonderful events in memory. Joe, as MC, deisplayed all his charisma and there is a wonderful video that captures him at his best. He won the best decorated golf cart award and kept the 'trophy' (a mini pull-back gold cart) on the kitchen counter! What a guy! He enjoyed life and friends.`,
        author: 'Bea Wright'
    },


    {
        text: `My first memory of Joe was a few months after moving into the St John's community. I attended a Board of Directors meeting in hopes of getting to know more about what this naeighborhood was all about. I was known to some at that point as "Betty" (this will make more sense later in the story). Towards the end of the meeting the current President was asking for Board of Director nominations and this great bass voice behind me nominates "Betsy"! Now, being new and surrounded by dozens of people, I had no idea if there were any Betsies in the room. But the slightlly uncomfortable silence soon made me realize that they might mean me! So I piped up, saying something like, "Do you mean Betty? Me?" Well, the conclusion of the story is that it was me and I ended up becoming quite enmeshed in the St Johns community. All due to Joe's exhuberance and his way of including anyone he ever met for a minute in everything. He made anyone he ever met feel like a lifelong friend in moments! I have always looked forward to Joyce's text message in spring asking for the first Friday Social at the beach, most of which included Joe providing the firewood for the firepit on cold evenings to make sure eveyone was warm and comfortable and would stay for a while. Joe made a person feel like they were the best person they could ever be. What a brilliant talent and he will be missed!!!!`,
        author: 'Bea Wright'
    },

    {
        text: `Back in the 90's, I was the owner of Around the Clock Answering Service. One winter day, (I only know that it was winter because Joe had on his ski coat), I was given a message to call Joe Flat toe,( that was how is was spelled)., regarding fiber optic cable. Little did I know that once he became a client taking calls for Harbenaux, Inc. that the mood of the service would change at the drop of a pin. Naming his business after his sons was a sign of his love for them. Joe wanted to use the phone number at the service while he started his company. Now,as to the mood of the service...I always knew when Joe would check in for messages, because whichever operator he was talking to would be laughing hysterically. He was never boring. My first story was when Joe moved into "Meadows at Elk Creek", he was ecstatic, because they had a tennis court. Now, back then, cell phone were fairly new on the market...Joe called & asked if I was busy & could he make some test calls, explaining that he wanted to know the range/distance he could be from the tower. He walked all over the property at "The Meadows" calling in, telling me how far out on the property he was. He also wanted his pager/beeper tested. This went on for quite awhile and his exuberance, when they both worked was enlightening! So now, Joe has his apartment...he calls in one day with yet another story to tell...He Loved the Acme on Elkton/Newark Rd., He had taken Harrison & Ben there to pick up some things for
        the kitchen. He was so joyful when telling me that the boys had picked out "Cow Towels & cow everything & he had a "Cow Kitchen" and they way he told it, couldn't help but smile but usually laugh, when talking to Joe, laughter was contagious.
        He would always fill us in on stories of he & Joyce & the boys...what they were doing for fun that particular day. He and Joyce were out riding bicycles, I think it was an event to raise money but it could have been just for pleasure...needless to say, he had to take Joyce to E.R. with a broken leg...nothing shook him. Then, when he bought the property on Old Field Point Rd., the excitement in his voice, because this was where he & Joyce were going to build their dream home and the boys would have a terrific neighborhood to grow up in. After the answering service closed down, we lost touch for awhile and then I would see him & Joyce, usually after church...it was like seeing a long lost friend. As far as those little gifts...he gave me a silk credit card case & to this day I still have it, although I retired it about a year ago as it was getting threadbare, and now even more glad that I held onto it.

        Every once in awhile I test my "Brain cells" to see if I remember all 200 accounts, numerically and when I get to #107, I will always remember YOU!
        Your Memory will live on!
        R.I.P. My Friend`,
        author: 'Dawn Cloud'
    },

    {
        text: `Dearest Joyce, Harrison, Ben, Josie, Patrick, and family, I wanted you to hear the beautiful story that Cody wrote for you. Cody thought is was too sad to read yesterday after Joe's service, but I know that you will appreciate hearing it, it comes from his heart.
        Too often we go through life without thanking the people that have helped us down the road of life. Joe Flatau was one of those people that I could not thank enough. Even when I annoyed him or made him mad because of something stupid I did, Joe still stood by me. He helped me through some of my darkest of moments. He was one of my very best friends. That's why his legacy must live on and his memory will be in our hearts forever.
        Joe once told me that it takes a village to raise a child, I didn't understand that at the time because I've been struggling to raise him all by myself, but I do now. As I walked around your house yesterday amongst all of Joe's family and friends, I heard so many people say, "Oh, your Cody's Mom, he's such a lovely, polite young man." It warms my heart to hear that my son is well liked by so many people. I'm so thankful that Cody had a good friend like Joe and all the other gentlemen in the neighborhood to help guide him, mentor him, and show him how to be a good young man. I'm am thankful for each and everyone of you, more than you will ever know, cause it does take a village to raise a child. RIP Joe.`,
        author: 'Debbi & Cody Norton-Russell'
    },

    {
        text: `I met Joe for one night at a wedding recently. my wife and I didn't know anyone at the wedding but the grooms father (Bill Mowbray), luckily we got to sit next to Joe and Joyce. needless to say we had a GREAT time with them, they really made our night. what a great guy, so full of life and stories. we had a lot of laughs. when I heard about him passing, it hit like a ton of bricks, which was weird for someone I only met for a few hrs. I guess he had that affect, made you feel like a long lost friend instantly. so sorry for the loss. drive it like you stole it, because you just never know.`,
        author: 'Vinny'
    },

    {
        text: `It is rare to find people with whom one can feel an instant bond. However, that is just what happened when I met Joe and Joyce. It is impossible to talk about Joe, without mentioning Joyce. Joe glowed when talking about Joyce and I could tell immediately that this man truly loved his wife, that it exuded his whole being. When my son Adam, Erin, my daughter-in-law, and my two grandchildren moved into St. John's Manor, they became quickly enmeshed into this unique welcoming community. Not only were they warmly accepted, but I was accepted as well. Joe and Joyce were so genuinely warm and friendly to me. I so looked forward to spending Happy Hour on the beach when I arrived on Fridays. It seemed that Joe and Joyce were the hosts of that fun time. Then there were the Superbowl parties, with boundless delicious foods, many prepared by Harrison, who is a great chef! The drink, games and laughter flowed throughout their beautiful home. Halloween became an extra special event as I looked forward to Joe's scary house with light and sound effects! What a talented man! I am so saddened to know that I will not see Joe's wonderful smile, or feel his kindness and hear his humor, but I feel so fortunate to have had the opportunity to have known him. I don't think Joe knew how much I cared for him, but I want Joyce to know how much I care for her and to know that I will be there for her in any way that I can.`,
        author: 'Fran Fleischhacker'
    },

    {
        text: `It is impossible for me to think of Joe without smiling. He truly was the most wonderful man to be around. So warm, so funny, so caring, and so incredibly intelligent. He had the gift of making everyone feel welcome and loved. I remember one summer in Avalon that Joe & Carol, Frank & Gail, Bill & Mary were visiting. They were arguing about some point of fact, when Joe, in an attempt to prove that he was right, declared "I was almost an astronaut". Everyone immediately laughed, including Joe. For years afterwards, whenever one of us wanted to lend weight to our opinion, we would say "I was almost an astronaut." And it would never fail to make everyone laugh and end the argument. The thing is, to me Joe, there was no almost about it. You really were an astronaut.`,
        author: 'Laura Burhans '
    },

    {
        text: `Joe had a lot of lawn to cut and he had this powerful cutting machine that he ran about 20 miles and hour diagonally one week, straight across the next. Did you ever notice that his otherwise perfectly straight rows had an occasional kink? That’s where he would take his hand off the controller, waive and smile at his neighbors passing by on Willow Oak Ct. Joe was a constant. You could rely on him for much more than a smile and a waive. I loved kibitzing with Joe and sharing how proud we were of our children. But I tried not to interrupt him when he was working. He was always doing something. A machine himself. But when he put down his tools, he was always engaging and genuinely interested in what was going on in the lives of his neighbors. He rejected negativity and made us all better for it. He was a man’s man and we all wished we could be more like Joe in some way or another. He was a giver, not a taker. He was welcoming, never judgmental and I think he wished everyone could be as satisfied with life as he was. I can’t help but smile when I think about him but I sure will miss seeing him on that cutting machine.`,
        author: 'Ed Phillips'
    },

    {
        text: `One of my favorite memories of Joe occurred at his and Joyce’s home in Elkton. Joe and Joyce, being the kind and generous people that they are, invited my husband and me to their home for the weekend. Josie and Ben were visiting from California (my husband is Josie’s brother) and Joe and Joyce invited us all to their home. This was a few years ago, before Josie and Ben were married or even engaged. I had never been to Joe and Joyce’s home, but I had heard many great stories of the their neighborhood, the boat, and the infamous apple cider donuts. Harrison also joined the festivities that weekend.
        Upon arrival, Joe had already set up a day of activities. He had bikes and helmets waiting for us to explore the area and a trail picked out for our adventure. It was a beautiful spring day and we rode for hours working up a sweat for the delicious dinner that he and Joyce were whipping up. When we came back to the house, Joe had golf clubs ready for chip and put. He also had a football on the grass for a game of catch, which turned into running football patterns. My husband and I live in center city Philadelphia, so running around in the open space of Elkton was so refreshing. Joe made us feel at home. He was especially excited about the new golf cart he recently purchased. He let us take it for a spin around the hood and even down to the water.
        That first evening as we were enjoying a great meal, Joe and Joyce told us that they had a surprise for us. They set up an entire neighborhood party down by the water for the next day so we could meet their friends, enjoy the water, see the boat, and perhaps most importantly, eat some Maryland crab! I could not believe all of the work that Joe and Joyce had put into this weekend. Brian and I had just met them and they showed us so much warmth and love. What struck me most, however, was their overflowing love for Ben and Harrison. They shared so many stories of Harrison and Ben growing up and it was clear that Harrison, Ben, and now Josie, were a highlight of their life. They loved the boys so much and even mentioned a few times how excited they were to get a daughter (again this was years before any engagement)!
        The party down by the water was a huge bash. The booze and crabs were abundant, Joe and Joyce’s friends were truly entertaining, and perhaps the highlight was when Joe lit up the party palm tree on his boat. It was a shin-dig for the history books.
        The morning after the big party, Joe and Joyce cooked the largest breakfast I have ever, and believe will ever, see in my life. If you can think of a breakfast food, it was there. Pancakes, waffles, eggs, hash browns, bagels, muffins, sausage, bacon, 4 different juices, coffee, tea, fruit, oatmeal, granola, yogurt…the list goes on and on. It was spectacular. We ate until our bellies bulged. What a great time! Then, Joe told us we must stop by the farm for apple cider donuts on our way out. We did and they were delicious!
        That weekend epitomized Joe. It was full of life, fun, energy and laughter. Friends and family, crabs and parties! Since then, every encounter I have had with Joe he has been kind, loving, and generous.
        The last time I saw Joe was about a month ago, he and Joyce stopped by our apartment in Philadelphia. They surprised us while we were in the middle of putting together an Ikea shelf. We have two daughters (2 years old and 8 months old) and all of their toys were cluttering the floor. Needless to say, our tiny city apartment was a mess. Joe came right in with a big smile on his face and said, “This is perfect. If you didn’t have all their toys out we wouldn’t get to see them in their element.” He always looked at the positive side of things, a trait that is rare and one I truly admire. He loved our two girls. As he left our apartment, Joe pulled out two hidden apple cider donuts. Classic!`,
        author: 'Ali Hinga Nevitt'
    },

    {
        text: `Charismatic, warm, cheerful, genuine...just some of the attributes that come to mind when I think about Joe. He and Joyce were always the perfect hosts. Joe was the best audience for me when Joyce would "twist my arm" to do my silly impersonations. I loved his laugh 😊 One of my funniest memories was the last time they took me skiing. Joe was so kind as to let me off in a mound of sea grass. Everyone seemed quite entertained watching me tangled up and struggling to get my skis on, while shaking long strands of grass off my arms! Once I was set, Joe did manage to get me up...you could say he was great like that! One could not help but be "up" around Joe. Thanks for the memories...`,
        author: 'Angela Mazza'
    },

    {
        text: `My first contact with Joe was via voicemail on Ben's phone several years ago. Josie, Ben, and I were joking around and I think I stole Ben's phone as he was leaving a message for his dad and yelled into the receiver with a funny voice. Later, I felt pretty embarrassed about it as I had yet to meet Ben's dad, but Ben assured me of Joe's good sense of humor, so I left it at that.
        Years later, I was fortunate enough to spend some time with Joe at Josie and Ben's wedding. Sure enough, I found him to be as good-humored and kind as Ben had described him. Joe and Joyce were so awesome throughout the wedding planning and were gracious enough to offer up their help in every way possible. One of the things I'll remember most from that week is Joe's crab cakes. He had them shipped in from the east coast and I remember people talking about them for weeks and weeks after the wedding—they were that good. I also have a very clear memory of Joe and Joyce lighting up the dance floor, even as some of the younger crowd (including myself) was losing steam. I was so impressed, I pulled out my phone and took a picture of the two boogieing down (I submitted this picture to the photos page). And although I hadn't known the two for very long, it was clear to me in that moment that they knew how to have a good time.
        I'll be thinking about the Flatau family lots and sending my love from California.`,
        author: 'Emily Nevitt'
    },

    {
        text: `I had so many great times with Joe, he was my friend, actually, he was one of my best friends. Joe taught me a lot of things, how to throw a football and correctly throw a baseball, and we were getting ready to start working on his trainer plane. We had a lot of great conversations and I'm really going to miss hanging out and talking to him. Joe always gave me good advice whenever I had a question or was concerned about something. Joe was my friend and I'm going miss him terribly. RIP Joe, I will never forget you or our fun times together.`,
        author: 'Cody Russell'
    },

    {
        text: `I only knew Joe for 3 or 4 years. We became fast friends at one of Adam and Erin Fleischhacker's parties.
        He was a great listener and so curious about 2 of my favorite hobbies: horse racing and stock options.
        One Saturday afternoon this year we sat together at the Fleischhacker dining room table and I taught him strategies used to trade stock options. He came prepared and was the perfect student, taking pages of notes and asking all the right questions. In the weeks that followed we emailed daily, back and forth, discussing the day's stock and option activity.
        Joe had a sharp mind and great sense of humor. In response to a silly cartoon I sent him last Thursday, this was his reply:
        "Surely you can't be serious ...
        I am serious . ... and don't call me Shirley! ;)"`,
        author: 'Phil Gerstein'
    },

    {
        text: `Joe, John and I used to get together for a ski adventure periodically. Our last trip together Feb. 7, 2014 we opted for reduced danger and went tubing instead of skiing due to injuries on the previous trip. The picture is of Joe, John and I at Bear Creek in PA.
        On the ski trip prior to this one, Joe somehow catapulted himself over his skis. He had a horrible Charlie Horse on his leg for months after that. Nobody knows how he actually did it, but Joe was extremely talented at those kinds of things.
        On one of our ski trips, we were at the top of a black diamond hill name Sasquatch. John and I went down the black diamond hill and Joe opted to go down a connector trail instead. When we got to the bottom we saw that the name of the connector trail that Joe took was named Baby Bear. To commemorate that event we decided to get Joe a teddy bear on skis, but it had to be the perfect one. We searched and found the most feminine looking teddy bear that we could find with skis, which we, of course, named Baby Bear. Joe always insisted that it was Menacing Bear.`,
        author: 'Ray Landes'
    },

    {
        text: `Although not in the technical sense any relation to me, Joe was at one time married to my "at-the time" wife's sister and for a num ber of wonderful years we enjoyed the closeness and love any family would embrace. Raising our children together and sharing countless good times remain a fond memory as well as a bittersweet reminder of just how fleeting life can be. Despite the differences over the years, I forever hold dear the special family vacations, late night poker games, holiday and birthday celebrations among varied other events our friends and family shared with us. I can only hope that comfort and peace find their way into the hearts of Joyce and my children's dear cousins Harrison and Ben.`,
        author: 'Frank Corado'
    },

    {
        text: `It was a cooler sun-filled morning up at Ithaca College in the hills of Central New York in early may of 2006. I was dredging through what was probably the worst hangover thus far in my life. I was a freshman fighting off a head-swell that only a night of red wine was responsible for. And now, on the morning of the last day of my first year at college, I frantically and desperately tried to pack my things before my sweet scary little mother showed up to take her baby home.
        Standing across the room, very calm and very collected, with all of his things impeccably packed and ready to go with not a spec of dirt or stain anywhere on his side, was my roommate Harrison; looking at me with a curious pity. Harrison and I could not be more different and have remained very good friends over the years. In college we performed sketch comedy together and have continued to write scripts and share in the creative process.
        On this particular morning Harrison was waiting for his dad to pick him up. And in he walked. I looked at both Joe and Harrison wondering wait, ok...wait, no resemblance but..I know Harrison is a fraternal twin so..yeah, ok actually it makes sense.
        Joe shook my hand like a man does and I shook it right back. I immediately wanted to grab a beer with this guy. Joe looked at me with a stern yet subtly humored expression. He was studying my situation, my bulging red eyes, the alcohol sweating off my forehead, the bedlam and squalor that made up my side of the room. He grinned. We all made small talk, I helped them take Harrison's belongings to their car and our interaction came to a close. I slapped Harrison 5 and went to shake Joe's hand. As the handshake commenced I had two $20 bills in my possession. I looked at the two Jacksons in my right hand palm then looked up at Joe. I insisted to refuse, saying "No, I can't accept," but he shot me a look that made me shut my mouth. He then grinned again and said "Go to the store, get whatever you had last night." And I thought "BOOM! THIS GUY GETS IT! I gotta have a beer with him! Ultimately, I never got to have that beer, but that morning, you better believe I went to the store and got my cure.`,
        author: 'Jake Alinikoff'
    },












]



function generateStories(){
    const mainContent = document.getElementById('main-content');

    storyData.forEach((story)=> {
        const storyContainer = document.createElement("div");
        storyContainer.className = "story-container";

        const storyAuthor = document.createElement("div");
        storyAuthor.className = "story-author";
        storyAuthor.innerHTML = 'By: ' + story.author;

        const storyText = document.createElement("div");
        storyText.className = "story-text";
        storyText.innerHTML = story.text;

        storyContainer.appendChild(storyAuthor);
        storyContainer.appendChild(storyText);
        mainContent.appendChild(storyContainer);
    });
}
